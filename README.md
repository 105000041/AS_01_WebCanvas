# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
#Report
Basic components
1.畫筆和橡皮擦
    可以改變顏色和粗細，也可以自訂
2.指標變化
    會在畫不同的物件時指標變成不同的樣子
3.文字輸入
    可以使用文字印章，輸入想要的文字就可以變成印章蓋上去
4.清除畫布
    在按下CLEAR之後，會清空畫布重新來過
5.上一步/下一步
    可以回復畫得不好的地方，變成上一步的完美圖畫；也可以按下一步，如果不小心按到上一步
6.形狀多多
    三角形、矩形、圓形任君挑選!
7.圖片工具
    在左下角選擇檔案上傳後，點擊手指工具，可以將圖片拉伸。
8.下載
    點download for free!之後，會自動將圖片以canvas的檔名下載。
9.Appearance(5%)
    認真排版。
10.其他功能
    可參考大師作品的影片，你也能成為大師

