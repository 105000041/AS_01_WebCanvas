var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var dataURL = false;
var x = 0;
var y = 0;
var old_x = 0;
var old_y = 0;
var begin=false;
var end=false;
var todo = "pencil";
var color="red";
var start_x=0;
var start_y=0;
var image_data_before=ctx.getImageData(0,0,canvas.height,canvas.width);
var img=0;
var texts=0;
var size=1;
var type=1;
var textsize=50;
var textfamily="Arial";
var music=0;
var download_img;
var flag=false;
var thick;
var cPushArray = new Array();
var cStep = -1;

ctx.fillStyle="#FFFFFF";
ctx.fillRect(0,0,canvas.width,canvas.height);
var cur=document.getElementById("cursortype");
cur.style.cursor="auto";

function cPush() {
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(document.getElementById('canvas').toDataURL());
} 

function change_text(a){
      if(a==10||a==50){
        textsize=a; 
        textsize=textsize.toString();
      }
      else{
        if(textfamily=='a') textfamily="Arial"
        else textfamily="Dancing Script"
      }
      ctx.font = textsize+"px"+textfamily;
}


var upload=document.querySelector('.upload');upload.addEventListener('change',function(e){
  let reader=new FileReader();
  reader.onload=function(event)
  {
    img=new Image();
    img.onload=function()
    {
      img.height=10;
      img.width=10;
    };
    img.src=this.result;
  };
  reader.readAsDataURL(this.files[0]);
});

function text() {
    texts=prompt("Please enter some text","TEXT");
    if(texts)
    {
      alert('just click~~~');
      cur.style.cursor="crosshair";
    }
    
}


function colorboard(good){
    color_or_type=good;
    changecolor(value);
}

function changecolor(value){
    color = value;
}

function changethickness(thickness){
    if(thickness == -1){
        thick = document.getElementById("thickness").value;
        ctx.lineWidth = thick;
    }
    else{
        ctx.lineWidth = thickness;
    }
}

function todolist(input) {
  todo = input;
  if(todo=="redo"){
    redo();
  }
  else if(todo=="undo"){
    undo();
  }
  else if(todo=="text"){
    text();
    //cur.style.cursor="crosshair";
  }
  else if(todo=="rectangle"){
    cur.style.cursor="crosshair";
  }
  else if(todo=="pencil"||todo=="eraser"){
    cur.style.cursor=" default";
  }
  else if(todo=="circle"){
    cur.style.cursor="crosshair";
  }
  else if(todo=="triangle"){
    cur.style.cursor="crosshair";
  }
}

function undo(){
  /*ctx.putImageData(image_data_before,0,0)*/
  if (cStep < cPushArray.length-1) {
    cStep++;
    var canvasPic = new Image();
    canvasPic.src = cPushArray[cStep];
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
}
}

function redo(){
  if (cStep > 0) {
    cStep--;
    var canvasPic = new Image();
    canvasPic.src = cPushArray[cStep];
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
}
}

function initial() {
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    canvas.addEventListener("mousedown", function (e) {
        down(e);
    });
    canvas.addEventListener("mouseup", function (e) {
        up(e);
    });
    canvas.addEventListener("mouseout", function (e) {
        out(e);
    });
    canvas.addEventListener("mousemove", function (e) {
        move(e);
    });
}

function up(e) {
  cPush();
  end=true;
  begin= false;
}

function down(e) {
  old_x = x;
  old_y = y;
  start_x=x;
  start_y=y;
  x = e.clientX - canvas.offsetLeft;
  y = e.clientY - canvas.offsetTop;
  image_data_before=ctx.getImageData(0,0,canvas.width,canvas.height);
  begin = true;
  if (todo == "pencil") {
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.fillRect(x,y,1,1);
    ctx.closePath();
  }
  else if(todo=="draw"){
    save();
  }
  else if(todo=="rectangle"){
    save();
  }
  else if(todo=="circle"){
    save();
  }
  else if(todo=="triangle"){
    save();
  }
  else if(todo=="text"){
    draw_text();
  }
}
function out(e) {
  end=false;
  begin = false;
}

function move(e) {
  old_x = x;
  old_y = y;
  x = e.clientX - canvas.offsetLeft;
  y = e.clientY - canvas.offsetTop;
  if (todo == "pencil") {
    pencil_draw();
  }
  else if(todo=="rectangle"){
    rectangle_draw();
  }
  else if(todo=="eraser"){
    eraser_draw();
  }
  else if(todo=="circle"){
    circle_draw();
  }
  else if(todo=="triangle"){
    triangle_draw();
  }
  else if(todo=="draw"){
    draw();
  }
}

function draw(){
  if(begin==true){
    load();
    ctx.beginPath();
    ctx.drawImage(img,start_x,start_y,x-start_x,y-start_y);
    ctx.closePath();
  }
  else if(end==true){
    ctx.beginPath();
    ctx.drawImage(img,start_x,start_y,x-start_x,y-start_y);
    end=false;
    ctx.closePath();
  }
}

function draw_text(){
    ctx.fillText(texts,x,y);
}

function triangle_draw(){
  if(begin==true){
    load();
    ctx.fillStyle=color;
    ctx.beginPath();
    ctx.moveTo(start_x,start_y);
    ctx.lineTo(x,y);
    ctx.lineTo(x-2*(x-start_x),y);
    ctx.lineTo(start_x,start_y);
    ctx.lineTo(x,y);
    ctx.fill();
    ctx.closePath();
  }
  else if(end==true){
    ctx.fillStyle=color;
    ctx.beginPath();
    ctx.moveTo(start_x,start_y);
    ctx.lineTo(x,y);
    ctx.lineTo(x-2*(x-start_x),y);
    ctx.lineTo(start_x,start_y);
    ctx.lineTo(x,y);
    end=false;
    ctx.fill();
    ctx.closePath();
    cPush();
  }
}
function circle_draw() {
  draw_or_not=true;
  if(begin==true){
    load();
    ctx.beginPath();
    ctx.fillStyle=color;
    ctx.arc(start_x,start_y,Math.abs(x-start_x),0,2*Math.PI);
    ctx.stroke();
    ctx.fill();
    ctx.closePath();
  }
  else if(end==true){
    ctx.beginPath();
    ctx.fillStyle=color;
    ctx.arc(start_x,start_y,Math.abs(x-start_x),0,2*Math.PI,true);
    ctx.stroke();
    end=false;
    ctx.fill();
    ctx.closePath();
    cPush();
  }
}

function rectangle_draw() {
  if(begin==true){
  load();
  ctx.beginPath();
  ctx.fillStyle=color;
  ctx.fillRect(start_x,start_y,x-start_x,y-start_y);
  ctx.closePath();
  }
  else if(end==true){
    ctx.beginPath();
    ctx.fillStyle=color;
    ctx.fillRect(start_x,start_y,x-start_x,y-start_y);
    end=false;
    ctx.closePath();
    cPush();
  }
}

function pencil_draw() {
  if (begin == true) {
    ctx.beginPath();
    ctx.moveTo(old_x, old_y);
    ctx.lineTo(x, y);
    ctx.strokeStyle = color;
    ctx.lineWidth = thick;
    ctx.stroke();
    ctx.closePath();
  
  }
}

function eraser_draw() {
  if(begin==true){
    ctx.beginPath();
    ctx.moveTo(old_x, old_y);
    ctx.lineTo(x, y); 
    ctx.strokeStyle="#FFFFFF";
    ctx.lineWidth = thick;
    ctx.stroke();
    ctx.closePath();
  }
}

function clean_canvas() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  document.getElementById("canvas").style.backgroundColor="#ffffff"

}

function save() {
  dataURL = canvas.toDataURL();
}

function load() {
  var imageObj = new Image();
  imageObj.onload = function () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(this, 0, 0);
  };
  imageObj.src = dataURL;
}